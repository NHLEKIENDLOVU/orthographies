# encodings


## UTF-8 character encodings


| character | name | unicode |
|-----------|------|---------|
| a         |      | |
| e         | | |
| i         | | |
| o         | | |
| u         | | |
| ƞ         | | |
| aⁿ
| iⁿ
| uⁿ
| oⁿ




<!--
## Lakota Orthographies


### Buechel


### Riggs


### Taylor


### White Hat


### Williamson

-->







## From 







## References


* White Hat, Sr., A. (1999). Reading and Writing the Lakota Language.
  Salt Lake City: The University of Utah Press. ISBN 978-0-87480-572-7

